InputFileName = ARGV[0] || "input"
SaveACopy = true

def process_data data
  # using ; as delimiter, must "unescape" it after splitting
  data
    .split(/(?<!\\);/)
    .map{ |file| file.gsub(/\\;/, ';') }
end

def process_regex data
  process_data(data).map{ |s| %r[#{s}] }
end

files_data, from_data, to_data = File.read(InputFileName).split("\n")
files_regex = process_regex files_data
from = process_regex from_data
to = process_data to_data

# checks all items in pwd recursively, and filters for files that matches any of the regex terms.
files = Dir.glob("#{Dir.pwd}/**/*").filter do |item|
  File.file?(item) && files_regex.any? { |regex| File.basename(item).match(regex) }
end

# verifies matching number of arguments
if from.size != to.size
  puts "\"from\" and \"to\" have different number of arguments, no modifications are made, exiting!"
  exit
end

files.each do |file|
  puts "Modifying #{file}"
  content = File.read(file)
  new_content = content.dup
  from.zip(to).each do |f, t|
    new_content.gsub! f, t
  end
  File.write(file, new_content)
  if SaveACopy
    content = "Original copy of \"#{File.basename(file)}\", modified by Replace++\n\n" + content
    File.write(file + "_copy", content)
  end
end

puts "Done!"
