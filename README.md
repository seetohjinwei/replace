# Replace+

A simple ruby script to perform "find and replace" operations on multiple files in one go.

Yes, there already exists multiple <a href="https://stackoverflow.com/questions/11392478/how-to-replace-a-string-in-multiple-files-in-linux-command-line">command line tool solutions</a> that solves this problem.

**However**, this script offers a couple of extra niceties.
1. Even if you have multiple replaces to be done *and* multiple files to be modified, you just have to run this once.
2. Input is parsed from a file so that multiple replacements will not end up as an extremely long bash command.
3. A copy of your original file is saved -- can be turned off by setting `SaveACopy` to be `false` in the script, in the 2nd line.

## Requirements
1. Working version of ruby. (only tested on ruby 3.0.0, however it *should* work on most recent versions)
3. `replace-plus.rb` file
3. 0 dependencies
4. An input file where the parameters are decided.

## Quick Usage
1. Move `replace-plus.rb` to your working directory.
2. Set up `input` file as specified below.
3. Run `ruby replace-plus.rb`.

A short example is also included in this repository.

## Full Details
### Input File
By default, the input file name is "input" (without quotes, and to be placed in the current working directory). However, one can specify another input file instead, by appending it as an argument when running the script `ruby path_to_replace-plus.rb alternative_input_file`.

All filepaths are relative to your current working directory.

### Formatting the Input
Only the first 3 lines (separated by a newline character) of the file is considered.

All arguments within each line is delimited by a semicolon (;). To escape a semicolon (;) within your own argument, use a backslash in front of your semicolon (\;). That is, `abc\;d` instead of `abc;d`.

The first line corresponds to the files to be modified. The arguments are treated as regex. Every file in the working directory (and in any subfolders) that match *any* of the arguments will be modified.

Second line corresponds to the "from" argument, this is treated as regex, so special characters must be escaped. The flavour of regex would be ruby (of course).

Third line corresponds to the "to" argument, this is treated as a string.

The second and third lines come in pairs. They must share the same number of arguments.

### Other Details
Remember to escape special characters for the first and second line of arguments. The flavour of regex is ruby (of course).

You might want to verify your regex before running the script. My preferred site is <a href="https://regex101.com">regex101.com</a>.

You might want to add '^' and '$' to the start and end of your file name respectively, to match the start and end of the regex search, to prevent unwanted actions like matching the saved copy.

The changes are processed from left to right and previous changes *will* affect future ones. So changes can be chained together, if so desired, if not, take note!
